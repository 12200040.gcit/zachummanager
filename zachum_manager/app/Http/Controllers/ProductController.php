<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index(){
        return view('products.index');
    }

    public function create(){
        return view('products.create');
    }

    // zachum controller below this
    public function zdashboard(){
        return view('admindashboard');
    }

    public function zcreatemenu(){
        return view('createmenu');
    }

    public function zevents(){
        return view('events');
    }

    public function zcreateEvent(){
        return view('createEvent');
    }
    
    public function zreservation(){
        return view('reservation');
    }

    public function zreservationPreview(){
        return view('reservationPreview');
    }
    public function zcustomers(){
        return view('customers');
    }
    public function zorders(){
        return view('orders');
    }
    public function zorderdetails(){
        return view('orderDetails');
    }
    public function zprofile(){
        return view('profile');
    }
    
}
