<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('login');
    // return View('admindashboard');
});

Route::get('/product',[ProductController::class, 'index'])->name('product.index');
Route::get('/product/create',[ProductController::class, 'create'])->name('product.create');



// zachum manager route
Route::get('/zdashboard',[ProductController::class, 'zdashboard'])->name('zdashboard');
Route::get('/zcreatemenu',[ProductController::class, 'zcreatemenu'])->name('zcreatemenu');
Route::get('/zevents',[ProductController::class, 'zevents'])->name('zevents');
Route::get('/zcreateEvent',[ProductController::class, 'zcreateEvent'])->name('zcreateEvent');
Route::get('/zreservation',[ProductController::class, 'zreservation'])->name('zreservation');
Route::get('/zreservationPreview',[ProductController::class, 'zreservationPreview'])->name('zreservationPreview');
Route::get('/zcustomers',[ProductController::class, 'zcustomers'])->name('zcustomers');
Route::get('/zorders',[ProductController::class, 'zorders'])->name('zorders');
Route::get('/zorderdetails',[ProductController::class, 'zorderdetails'])->name('zorderdetails');
Route::get('/zprofile',[ProductController::class, 'zprofile'])->name('zprofile');








